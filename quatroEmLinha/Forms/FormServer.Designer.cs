﻿namespace quatroEmLinha
{
    partial class FormServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cBoxDebugMode = new System.Windows.Forms.CheckBox();
            this.numUDPort = new System.Windows.Forms.NumericUpDown();
            this.numUDMax = new System.Windows.Forms.NumericUpDown();
            this.btnListen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numUDPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDMax)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(13, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(13, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Maximum Connections:";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(13, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Debug Mode?";
            // 
            // cBoxDebugMode
            // 
            this.cBoxDebugMode.AutoSize = true;
            this.cBoxDebugMode.Enabled = false;
            this.cBoxDebugMode.Location = new System.Drawing.Point(129, 88);
            this.cBoxDebugMode.Name = "cBoxDebugMode";
            this.cBoxDebugMode.Size = new System.Drawing.Size(15, 14);
            this.cBoxDebugMode.TabIndex = 1;
            this.cBoxDebugMode.UseVisualStyleBackColor = true;
            // 
            // numUDPort
            // 
            this.numUDPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.numUDPort.Location = new System.Drawing.Point(124, 17);
            this.numUDPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numUDPort.Minimum = new decimal(new int[] {
            1025,
            0,
            0,
            0});
            this.numUDPort.Name = "numUDPort";
            this.numUDPort.Size = new System.Drawing.Size(120, 26);
            this.numUDPort.TabIndex = 2;
            this.numUDPort.Value = new decimal(new int[] {
            1025,
            0,
            0,
            0});
            // 
            // numUDMax
            // 
            this.numUDMax.Enabled = false;
            this.numUDMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.numUDMax.Location = new System.Drawing.Point(192, 48);
            this.numUDMax.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numUDMax.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUDMax.Name = "numUDMax";
            this.numUDMax.Size = new System.Drawing.Size(52, 26);
            this.numUDMax.TabIndex = 2;
            this.numUDMax.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUDMax.Visible = false;
            // 
            // btnListen
            // 
            this.btnListen.Location = new System.Drawing.Point(17, 127);
            this.btnListen.Name = "btnListen";
            this.btnListen.Size = new System.Drawing.Size(227, 35);
            this.btnListen.TabIndex = 3;
            this.btnListen.Text = "Listen";
            this.btnListen.UseVisualStyleBackColor = true;
            this.btnListen.Click += new System.EventHandler(this.btnListen_Click);
            // 
            // FormServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 174);
            this.Controls.Add(this.btnListen);
            this.Controls.Add(this.numUDMax);
            this.Controls.Add(this.numUDPort);
            this.Controls.Add(this.cBoxDebugMode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormServer";
            this.Text = "Server Settings";
            ((System.ComponentModel.ISupportInitialize)(this.numUDPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDMax)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cBoxDebugMode;
        private System.Windows.Forms.NumericUpDown numUDPort;
        private System.Windows.Forms.NumericUpDown numUDMax;
        private System.Windows.Forms.Button btnListen;
    }
}