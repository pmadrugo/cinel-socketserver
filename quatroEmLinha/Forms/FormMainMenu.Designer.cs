﻿namespace quatroEmLinha
{
    partial class FormMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hostAGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToAGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hostAGameToolStripMenuItem,
            this.connectToAGameToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(387, 280);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hostAGameToolStripMenuItem
            // 
            this.hostAGameToolStripMenuItem.Name = "hostAGameToolStripMenuItem";
            this.hostAGameToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 70, 4, 0);
            this.hostAGameToolStripMenuItem.Size = new System.Drawing.Size(386, 89);
            this.hostAGameToolStripMenuItem.Text = "Host a Game";
            this.hostAGameToolStripMenuItem.Click += new System.EventHandler(this.hostAGameToolStripMenuItem_Click);
            // 
            // connectToAGameToolStripMenuItem
            // 
            this.connectToAGameToolStripMenuItem.Name = "connectToAGameToolStripMenuItem";
            this.connectToAGameToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 70, 4, 0);
            this.connectToAGameToolStripMenuItem.Size = new System.Drawing.Size(386, 89);
            this.connectToAGameToolStripMenuItem.Text = "Connect to a Game";
            this.connectToAGameToolStripMenuItem.Click += new System.EventHandler(this.connectToAGameToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 70, 4, 0);
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(386, 89);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // FormMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 280);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMainMenu";
            this.Text = "Connect Four Main Menu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hostAGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToAGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}