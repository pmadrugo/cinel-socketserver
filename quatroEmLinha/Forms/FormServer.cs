﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using quatroEmLinha.Server;

namespace quatroEmLinha
{
    public partial class FormServer : Form
    {
        public FormServer()
        {
            InitializeComponent();
        }

        private void btnListen_Click(object sender, EventArgs e)
        {
            // Creates a new Server object
            Server.Server server = new Server.Server(Convert.ToUInt32(numUDPort.Value));

            // Commences listening
            server.StartListen();

            // Calls a pre-filled connection form so player can insert nickname
            FormConnect client = new FormConnect(true, Convert.ToInt32(numUDPort.Value));
            client.ShowDialog();
            
            // When everything is closed, terminates the socket and all associated connections
            server.ServerSocket.Close();
            server.ClientsList.ForEach( (c) => c.ConnectionData.Socket.Close());
        }
    }
}
