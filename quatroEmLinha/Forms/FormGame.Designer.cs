﻿namespace quatroEmLinha
{
    partial class FormGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spltGame_Msg = new System.Windows.Forms.SplitContainer();
            this.spltGame_Buttons = new System.Windows.Forms.SplitContainer();
            this.lbl60 = new System.Windows.Forms.Label();
            this.lbl61 = new System.Windows.Forms.Label();
            this.lbl62 = new System.Windows.Forms.Label();
            this.lbl63 = new System.Windows.Forms.Label();
            this.lbl50 = new System.Windows.Forms.Label();
            this.lbl64 = new System.Windows.Forms.Label();
            this.lbl51 = new System.Windows.Forms.Label();
            this.lbl65 = new System.Windows.Forms.Label();
            this.lbl52 = new System.Windows.Forms.Label();
            this.lbl53 = new System.Windows.Forms.Label();
            this.lbl40 = new System.Windows.Forms.Label();
            this.lbl54 = new System.Windows.Forms.Label();
            this.lbl41 = new System.Windows.Forms.Label();
            this.lbl55 = new System.Windows.Forms.Label();
            this.lbl42 = new System.Windows.Forms.Label();
            this.lbl43 = new System.Windows.Forms.Label();
            this.lbl30 = new System.Windows.Forms.Label();
            this.lbl44 = new System.Windows.Forms.Label();
            this.lbl31 = new System.Windows.Forms.Label();
            this.lbl45 = new System.Windows.Forms.Label();
            this.lbl32 = new System.Windows.Forms.Label();
            this.lbl33 = new System.Windows.Forms.Label();
            this.lbl20 = new System.Windows.Forms.Label();
            this.lbl34 = new System.Windows.Forms.Label();
            this.lbl21 = new System.Windows.Forms.Label();
            this.lbl35 = new System.Windows.Forms.Label();
            this.lbl22 = new System.Windows.Forms.Label();
            this.lbl23 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl24 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl00 = new System.Windows.Forms.Label();
            this.lbl25 = new System.Windows.Forms.Label();
            this.lbl01 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl02 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl03 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.lbl04 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl05 = new System.Windows.Forms.Label();
            this.btnCol7 = new System.Windows.Forms.Button();
            this.btnCol6 = new System.Windows.Forms.Button();
            this.btnCol5 = new System.Windows.Forms.Button();
            this.btnCol4 = new System.Windows.Forms.Button();
            this.btnCol3 = new System.Windows.Forms.Button();
            this.btnCol2 = new System.Windows.Forms.Button();
            this.btnCol1 = new System.Windows.Forms.Button();
            this.spltChat_MsgSend = new System.Windows.Forms.SplitContainer();
            this.txtChat = new System.Windows.Forms.TextBox();
            this.btnMsgSend = new System.Windows.Forms.Button();
            this.txtMsgSend = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.spltGame_Msg)).BeginInit();
            this.spltGame_Msg.Panel1.SuspendLayout();
            this.spltGame_Msg.Panel2.SuspendLayout();
            this.spltGame_Msg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltGame_Buttons)).BeginInit();
            this.spltGame_Buttons.Panel1.SuspendLayout();
            this.spltGame_Buttons.Panel2.SuspendLayout();
            this.spltGame_Buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltChat_MsgSend)).BeginInit();
            this.spltChat_MsgSend.Panel1.SuspendLayout();
            this.spltChat_MsgSend.Panel2.SuspendLayout();
            this.spltChat_MsgSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // spltGame_Msg
            // 
            this.spltGame_Msg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltGame_Msg.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.spltGame_Msg.IsSplitterFixed = true;
            this.spltGame_Msg.Location = new System.Drawing.Point(0, 0);
            this.spltGame_Msg.Name = "spltGame_Msg";
            // 
            // spltGame_Msg.Panel1
            // 
            this.spltGame_Msg.Panel1.Controls.Add(this.spltGame_Buttons);
            // 
            // spltGame_Msg.Panel2
            // 
            this.spltGame_Msg.Panel2.Controls.Add(this.spltChat_MsgSend);
            this.spltGame_Msg.Size = new System.Drawing.Size(665, 460);
            this.spltGame_Msg.SplitterDistance = 484;
            this.spltGame_Msg.TabIndex = 0;
            // 
            // spltGame_Buttons
            // 
            this.spltGame_Buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltGame_Buttons.IsSplitterFixed = true;
            this.spltGame_Buttons.Location = new System.Drawing.Point(0, 0);
            this.spltGame_Buttons.Name = "spltGame_Buttons";
            this.spltGame_Buttons.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltGame_Buttons.Panel1
            // 
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl60);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl61);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl62);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl63);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl50);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl64);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl51);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl65);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl52);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl53);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl40);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl54);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl41);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl55);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl42);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl43);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl30);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl44);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl31);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl45);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl32);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl33);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl20);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl34);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl21);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl35);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl22);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl23);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl10);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl24);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl11);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl00);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl25);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl01);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl12);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl02);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl13);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl03);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl14);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl04);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl15);
            this.spltGame_Buttons.Panel1.Controls.Add(this.lbl05);
            // 
            // spltGame_Buttons.Panel2
            // 
            this.spltGame_Buttons.Panel2.Controls.Add(this.btnCol7);
            this.spltGame_Buttons.Panel2.Controls.Add(this.btnCol6);
            this.spltGame_Buttons.Panel2.Controls.Add(this.btnCol5);
            this.spltGame_Buttons.Panel2.Controls.Add(this.btnCol4);
            this.spltGame_Buttons.Panel2.Controls.Add(this.btnCol3);
            this.spltGame_Buttons.Panel2.Controls.Add(this.btnCol2);
            this.spltGame_Buttons.Panel2.Controls.Add(this.btnCol1);
            this.spltGame_Buttons.Size = new System.Drawing.Size(484, 460);
            this.spltGame_Buttons.SplitterDistance = 418;
            this.spltGame_Buttons.TabIndex = 0;
            // 
            // lbl60
            // 
            this.lbl60.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl60.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl60.Location = new System.Drawing.Point(413, 354);
            this.lbl60.Margin = new System.Windows.Forms.Padding(0);
            this.lbl60.Name = "lbl60";
            this.lbl60.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl60.Size = new System.Drawing.Size(54, 58);
            this.lbl60.TabIndex = 3;
            this.lbl60.Text = "O";
            this.lbl60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl61
            // 
            this.lbl61.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl61.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl61.Location = new System.Drawing.Point(413, 285);
            this.lbl61.Margin = new System.Windows.Forms.Padding(0);
            this.lbl61.Name = "lbl61";
            this.lbl61.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl61.Size = new System.Drawing.Size(54, 58);
            this.lbl61.TabIndex = 25;
            this.lbl61.Text = "O";
            this.lbl61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl62
            // 
            this.lbl62.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl62.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl62.Location = new System.Drawing.Point(413, 216);
            this.lbl62.Margin = new System.Windows.Forms.Padding(0);
            this.lbl62.Name = "lbl62";
            this.lbl62.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl62.Size = new System.Drawing.Size(54, 58);
            this.lbl62.TabIndex = 26;
            this.lbl62.Text = "O";
            this.lbl62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl63
            // 
            this.lbl63.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl63.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl63.Location = new System.Drawing.Point(413, 147);
            this.lbl63.Margin = new System.Windows.Forms.Padding(0);
            this.lbl63.Name = "lbl63";
            this.lbl63.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl63.Size = new System.Drawing.Size(54, 58);
            this.lbl63.TabIndex = 27;
            this.lbl63.Text = "O";
            this.lbl63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl50
            // 
            this.lbl50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl50.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl50.Location = new System.Drawing.Point(347, 354);
            this.lbl50.Margin = new System.Windows.Forms.Padding(0);
            this.lbl50.Name = "lbl50";
            this.lbl50.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl50.Size = new System.Drawing.Size(54, 58);
            this.lbl50.TabIndex = 28;
            this.lbl50.Text = "O";
            this.lbl50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl64
            // 
            this.lbl64.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl64.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl64.Location = new System.Drawing.Point(413, 78);
            this.lbl64.Margin = new System.Windows.Forms.Padding(0);
            this.lbl64.Name = "lbl64";
            this.lbl64.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl64.Size = new System.Drawing.Size(54, 58);
            this.lbl64.TabIndex = 29;
            this.lbl64.Text = "O";
            this.lbl64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl51
            // 
            this.lbl51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl51.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl51.Location = new System.Drawing.Point(347, 285);
            this.lbl51.Margin = new System.Windows.Forms.Padding(0);
            this.lbl51.Name = "lbl51";
            this.lbl51.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl51.Size = new System.Drawing.Size(54, 58);
            this.lbl51.TabIndex = 30;
            this.lbl51.Text = "O";
            this.lbl51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl65
            // 
            this.lbl65.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl65.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl65.Location = new System.Drawing.Point(413, 9);
            this.lbl65.Margin = new System.Windows.Forms.Padding(0);
            this.lbl65.Name = "lbl65";
            this.lbl65.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl65.Size = new System.Drawing.Size(54, 58);
            this.lbl65.TabIndex = 31;
            this.lbl65.Text = "O";
            this.lbl65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl52
            // 
            this.lbl52.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl52.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl52.Location = new System.Drawing.Point(347, 216);
            this.lbl52.Margin = new System.Windows.Forms.Padding(0);
            this.lbl52.Name = "lbl52";
            this.lbl52.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl52.Size = new System.Drawing.Size(54, 58);
            this.lbl52.TabIndex = 32;
            this.lbl52.Text = "O";
            this.lbl52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl53
            // 
            this.lbl53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl53.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl53.Location = new System.Drawing.Point(347, 147);
            this.lbl53.Margin = new System.Windows.Forms.Padding(0);
            this.lbl53.Name = "lbl53";
            this.lbl53.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl53.Size = new System.Drawing.Size(54, 58);
            this.lbl53.TabIndex = 34;
            this.lbl53.Text = "O";
            this.lbl53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl40
            // 
            this.lbl40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl40.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl40.Location = new System.Drawing.Point(281, 354);
            this.lbl40.Margin = new System.Windows.Forms.Padding(0);
            this.lbl40.Name = "lbl40";
            this.lbl40.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl40.Size = new System.Drawing.Size(54, 58);
            this.lbl40.TabIndex = 43;
            this.lbl40.Text = "O";
            this.lbl40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl54
            // 
            this.lbl54.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl54.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl54.Location = new System.Drawing.Point(347, 78);
            this.lbl54.Margin = new System.Windows.Forms.Padding(0);
            this.lbl54.Name = "lbl54";
            this.lbl54.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl54.Size = new System.Drawing.Size(54, 58);
            this.lbl54.TabIndex = 35;
            this.lbl54.Text = "O";
            this.lbl54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl41
            // 
            this.lbl41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl41.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl41.Location = new System.Drawing.Point(281, 285);
            this.lbl41.Margin = new System.Windows.Forms.Padding(0);
            this.lbl41.Name = "lbl41";
            this.lbl41.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl41.Size = new System.Drawing.Size(54, 58);
            this.lbl41.TabIndex = 36;
            this.lbl41.Text = "O";
            this.lbl41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl55
            // 
            this.lbl55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl55.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl55.Location = new System.Drawing.Point(347, 9);
            this.lbl55.Margin = new System.Windows.Forms.Padding(0);
            this.lbl55.Name = "lbl55";
            this.lbl55.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl55.Size = new System.Drawing.Size(54, 58);
            this.lbl55.TabIndex = 37;
            this.lbl55.Text = "O";
            this.lbl55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl42
            // 
            this.lbl42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl42.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl42.Location = new System.Drawing.Point(281, 216);
            this.lbl42.Margin = new System.Windows.Forms.Padding(0);
            this.lbl42.Name = "lbl42";
            this.lbl42.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl42.Size = new System.Drawing.Size(54, 58);
            this.lbl42.TabIndex = 38;
            this.lbl42.Text = "O";
            this.lbl42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl43
            // 
            this.lbl43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl43.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl43.Location = new System.Drawing.Point(281, 147);
            this.lbl43.Margin = new System.Windows.Forms.Padding(0);
            this.lbl43.Name = "lbl43";
            this.lbl43.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl43.Size = new System.Drawing.Size(54, 58);
            this.lbl43.TabIndex = 39;
            this.lbl43.Text = "O";
            this.lbl43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl30
            // 
            this.lbl30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl30.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl30.Location = new System.Drawing.Point(215, 354);
            this.lbl30.Margin = new System.Windows.Forms.Padding(0);
            this.lbl30.Name = "lbl30";
            this.lbl30.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl30.Size = new System.Drawing.Size(54, 58);
            this.lbl30.TabIndex = 40;
            this.lbl30.Text = "O";
            this.lbl30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl44
            // 
            this.lbl44.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl44.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl44.Location = new System.Drawing.Point(281, 78);
            this.lbl44.Margin = new System.Windows.Forms.Padding(0);
            this.lbl44.Name = "lbl44";
            this.lbl44.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl44.Size = new System.Drawing.Size(54, 58);
            this.lbl44.TabIndex = 41;
            this.lbl44.Text = "O";
            this.lbl44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl31
            // 
            this.lbl31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl31.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl31.Location = new System.Drawing.Point(215, 285);
            this.lbl31.Margin = new System.Windows.Forms.Padding(0);
            this.lbl31.Name = "lbl31";
            this.lbl31.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl31.Size = new System.Drawing.Size(54, 58);
            this.lbl31.TabIndex = 42;
            this.lbl31.Text = "O";
            this.lbl31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl45
            // 
            this.lbl45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl45.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl45.Location = new System.Drawing.Point(281, 9);
            this.lbl45.Margin = new System.Windows.Forms.Padding(0);
            this.lbl45.Name = "lbl45";
            this.lbl45.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl45.Size = new System.Drawing.Size(54, 58);
            this.lbl45.TabIndex = 24;
            this.lbl45.Text = "O";
            this.lbl45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl32
            // 
            this.lbl32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl32.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl32.Location = new System.Drawing.Point(215, 216);
            this.lbl32.Margin = new System.Windows.Forms.Padding(0);
            this.lbl32.Name = "lbl32";
            this.lbl32.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl32.Size = new System.Drawing.Size(54, 58);
            this.lbl32.TabIndex = 23;
            this.lbl32.Text = "O";
            this.lbl32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl33
            // 
            this.lbl33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl33.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl33.Location = new System.Drawing.Point(215, 147);
            this.lbl33.Margin = new System.Windows.Forms.Padding(0);
            this.lbl33.Name = "lbl33";
            this.lbl33.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl33.Size = new System.Drawing.Size(54, 58);
            this.lbl33.TabIndex = 22;
            this.lbl33.Text = "O";
            this.lbl33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl20
            // 
            this.lbl20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl20.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl20.Location = new System.Drawing.Point(149, 354);
            this.lbl20.Margin = new System.Windows.Forms.Padding(0);
            this.lbl20.Name = "lbl20";
            this.lbl20.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl20.Size = new System.Drawing.Size(54, 58);
            this.lbl20.TabIndex = 21;
            this.lbl20.Text = "O";
            this.lbl20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl34
            // 
            this.lbl34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl34.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl34.Location = new System.Drawing.Point(215, 78);
            this.lbl34.Margin = new System.Windows.Forms.Padding(0);
            this.lbl34.Name = "lbl34";
            this.lbl34.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl34.Size = new System.Drawing.Size(54, 58);
            this.lbl34.TabIndex = 4;
            this.lbl34.Text = "O";
            this.lbl34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl21
            // 
            this.lbl21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl21.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl21.Location = new System.Drawing.Point(149, 285);
            this.lbl21.Margin = new System.Windows.Forms.Padding(0);
            this.lbl21.Name = "lbl21";
            this.lbl21.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl21.Size = new System.Drawing.Size(54, 58);
            this.lbl21.TabIndex = 5;
            this.lbl21.Text = "O";
            this.lbl21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl35
            // 
            this.lbl35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl35.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl35.Location = new System.Drawing.Point(215, 9);
            this.lbl35.Margin = new System.Windows.Forms.Padding(0);
            this.lbl35.Name = "lbl35";
            this.lbl35.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl35.Size = new System.Drawing.Size(54, 58);
            this.lbl35.TabIndex = 6;
            this.lbl35.Text = "O";
            this.lbl35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl22
            // 
            this.lbl22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl22.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl22.Location = new System.Drawing.Point(149, 216);
            this.lbl22.Margin = new System.Windows.Forms.Padding(0);
            this.lbl22.Name = "lbl22";
            this.lbl22.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl22.Size = new System.Drawing.Size(54, 58);
            this.lbl22.TabIndex = 7;
            this.lbl22.Text = "O";
            this.lbl22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl23
            // 
            this.lbl23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl23.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl23.Location = new System.Drawing.Point(149, 147);
            this.lbl23.Margin = new System.Windows.Forms.Padding(0);
            this.lbl23.Name = "lbl23";
            this.lbl23.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl23.Size = new System.Drawing.Size(54, 58);
            this.lbl23.TabIndex = 8;
            this.lbl23.Text = "O";
            this.lbl23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl10
            // 
            this.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl10.Location = new System.Drawing.Point(83, 354);
            this.lbl10.Margin = new System.Windows.Forms.Padding(0);
            this.lbl10.Name = "lbl10";
            this.lbl10.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl10.Size = new System.Drawing.Size(54, 58);
            this.lbl10.TabIndex = 9;
            this.lbl10.Text = "O";
            this.lbl10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl24
            // 
            this.lbl24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl24.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl24.Location = new System.Drawing.Point(149, 78);
            this.lbl24.Margin = new System.Windows.Forms.Padding(0);
            this.lbl24.Name = "lbl24";
            this.lbl24.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl24.Size = new System.Drawing.Size(54, 58);
            this.lbl24.TabIndex = 10;
            this.lbl24.Text = "O";
            this.lbl24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl11
            // 
            this.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl11.Location = new System.Drawing.Point(83, 285);
            this.lbl11.Margin = new System.Windows.Forms.Padding(0);
            this.lbl11.Name = "lbl11";
            this.lbl11.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl11.Size = new System.Drawing.Size(54, 58);
            this.lbl11.TabIndex = 11;
            this.lbl11.Text = "O";
            this.lbl11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl00
            // 
            this.lbl00.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl00.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl00.Location = new System.Drawing.Point(17, 354);
            this.lbl00.Margin = new System.Windows.Forms.Padding(0);
            this.lbl00.Name = "lbl00";
            this.lbl00.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl00.Size = new System.Drawing.Size(54, 58);
            this.lbl00.TabIndex = 12;
            this.lbl00.Text = "O";
            this.lbl00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl25
            // 
            this.lbl25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl25.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl25.Location = new System.Drawing.Point(149, 9);
            this.lbl25.Margin = new System.Windows.Forms.Padding(0);
            this.lbl25.Name = "lbl25";
            this.lbl25.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl25.Size = new System.Drawing.Size(54, 58);
            this.lbl25.TabIndex = 13;
            this.lbl25.Text = "O";
            this.lbl25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl01
            // 
            this.lbl01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl01.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl01.Location = new System.Drawing.Point(17, 285);
            this.lbl01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl01.Name = "lbl01";
            this.lbl01.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl01.Size = new System.Drawing.Size(54, 58);
            this.lbl01.TabIndex = 14;
            this.lbl01.Text = "O";
            this.lbl01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl12
            // 
            this.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl12.Location = new System.Drawing.Point(83, 216);
            this.lbl12.Margin = new System.Windows.Forms.Padding(0);
            this.lbl12.Name = "lbl12";
            this.lbl12.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl12.Size = new System.Drawing.Size(54, 58);
            this.lbl12.TabIndex = 15;
            this.lbl12.Text = "O";
            this.lbl12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl02
            // 
            this.lbl02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl02.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl02.Location = new System.Drawing.Point(17, 216);
            this.lbl02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl02.Name = "lbl02";
            this.lbl02.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl02.Size = new System.Drawing.Size(54, 58);
            this.lbl02.TabIndex = 16;
            this.lbl02.Text = "O";
            this.lbl02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl13
            // 
            this.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl13.Location = new System.Drawing.Point(83, 147);
            this.lbl13.Margin = new System.Windows.Forms.Padding(0);
            this.lbl13.Name = "lbl13";
            this.lbl13.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl13.Size = new System.Drawing.Size(54, 58);
            this.lbl13.TabIndex = 17;
            this.lbl13.Text = "O";
            this.lbl13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl03
            // 
            this.lbl03.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl03.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl03.Location = new System.Drawing.Point(17, 147);
            this.lbl03.Margin = new System.Windows.Forms.Padding(0);
            this.lbl03.Name = "lbl03";
            this.lbl03.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl03.Size = new System.Drawing.Size(54, 58);
            this.lbl03.TabIndex = 18;
            this.lbl03.Text = "O";
            this.lbl03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl14
            // 
            this.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl14.Location = new System.Drawing.Point(83, 78);
            this.lbl14.Margin = new System.Windows.Forms.Padding(0);
            this.lbl14.Name = "lbl14";
            this.lbl14.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl14.Size = new System.Drawing.Size(54, 58);
            this.lbl14.TabIndex = 19;
            this.lbl14.Text = "O";
            this.lbl14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl04
            // 
            this.lbl04.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl04.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl04.Location = new System.Drawing.Point(17, 78);
            this.lbl04.Margin = new System.Windows.Forms.Padding(0);
            this.lbl04.Name = "lbl04";
            this.lbl04.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl04.Size = new System.Drawing.Size(54, 58);
            this.lbl04.TabIndex = 20;
            this.lbl04.Text = "O";
            this.lbl04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl15
            // 
            this.lbl15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl15.Location = new System.Drawing.Point(83, 9);
            this.lbl15.Margin = new System.Windows.Forms.Padding(0);
            this.lbl15.Name = "lbl15";
            this.lbl15.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl15.Size = new System.Drawing.Size(54, 58);
            this.lbl15.TabIndex = 33;
            this.lbl15.Text = "O";
            this.lbl15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl05
            // 
            this.lbl05.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl05.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F);
            this.lbl05.Location = new System.Drawing.Point(17, 9);
            this.lbl05.Margin = new System.Windows.Forms.Padding(0);
            this.lbl05.Name = "lbl05";
            this.lbl05.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl05.Size = new System.Drawing.Size(54, 58);
            this.lbl05.TabIndex = 44;
            this.lbl05.Text = "O";
            this.lbl05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCol7
            // 
            this.btnCol7.Location = new System.Drawing.Point(413, 3);
            this.btnCol7.Name = "btnCol7";
            this.btnCol7.Size = new System.Drawing.Size(54, 31);
            this.btnCol7.TabIndex = 1;
            this.btnCol7.UseVisualStyleBackColor = true;
            this.btnCol7.Click += new System.EventHandler(this.btnCol7_Click);
            // 
            // btnCol6
            // 
            this.btnCol6.Location = new System.Drawing.Point(347, 3);
            this.btnCol6.Name = "btnCol6";
            this.btnCol6.Size = new System.Drawing.Size(54, 31);
            this.btnCol6.TabIndex = 1;
            this.btnCol6.UseVisualStyleBackColor = true;
            this.btnCol6.Click += new System.EventHandler(this.btnCol6_Click);
            // 
            // btnCol5
            // 
            this.btnCol5.Location = new System.Drawing.Point(281, 3);
            this.btnCol5.Name = "btnCol5";
            this.btnCol5.Size = new System.Drawing.Size(54, 31);
            this.btnCol5.TabIndex = 1;
            this.btnCol5.UseVisualStyleBackColor = true;
            this.btnCol5.Click += new System.EventHandler(this.btnCol5_Click);
            // 
            // btnCol4
            // 
            this.btnCol4.Location = new System.Drawing.Point(215, 3);
            this.btnCol4.Name = "btnCol4";
            this.btnCol4.Size = new System.Drawing.Size(54, 31);
            this.btnCol4.TabIndex = 1;
            this.btnCol4.UseVisualStyleBackColor = true;
            this.btnCol4.Click += new System.EventHandler(this.btnCol4_Click);
            // 
            // btnCol3
            // 
            this.btnCol3.Location = new System.Drawing.Point(149, 3);
            this.btnCol3.Name = "btnCol3";
            this.btnCol3.Size = new System.Drawing.Size(54, 31);
            this.btnCol3.TabIndex = 1;
            this.btnCol3.UseVisualStyleBackColor = true;
            this.btnCol3.Click += new System.EventHandler(this.btnCol3_Click);
            // 
            // btnCol2
            // 
            this.btnCol2.Location = new System.Drawing.Point(83, 3);
            this.btnCol2.Name = "btnCol2";
            this.btnCol2.Size = new System.Drawing.Size(54, 31);
            this.btnCol2.TabIndex = 1;
            this.btnCol2.UseVisualStyleBackColor = true;
            this.btnCol2.Click += new System.EventHandler(this.btnCol2_Click);
            // 
            // btnCol1
            // 
            this.btnCol1.Location = new System.Drawing.Point(17, 3);
            this.btnCol1.Name = "btnCol1";
            this.btnCol1.Size = new System.Drawing.Size(54, 31);
            this.btnCol1.TabIndex = 1;
            this.btnCol1.UseVisualStyleBackColor = true;
            this.btnCol1.Click += new System.EventHandler(this.btnCol1_Click);
            // 
            // spltChat_MsgSend
            // 
            this.spltChat_MsgSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltChat_MsgSend.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.spltChat_MsgSend.IsSplitterFixed = true;
            this.spltChat_MsgSend.Location = new System.Drawing.Point(0, 0);
            this.spltChat_MsgSend.Name = "spltChat_MsgSend";
            this.spltChat_MsgSend.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltChat_MsgSend.Panel1
            // 
            this.spltChat_MsgSend.Panel1.Controls.Add(this.txtChat);
            // 
            // spltChat_MsgSend.Panel2
            // 
            this.spltChat_MsgSend.Panel2.Controls.Add(this.btnMsgSend);
            this.spltChat_MsgSend.Panel2.Controls.Add(this.txtMsgSend);
            this.spltChat_MsgSend.Size = new System.Drawing.Size(177, 460);
            this.spltChat_MsgSend.SplitterDistance = 351;
            this.spltChat_MsgSend.TabIndex = 0;
            // 
            // txtChat
            // 
            this.txtChat.BackColor = System.Drawing.SystemColors.Window;
            this.txtChat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtChat.Location = new System.Drawing.Point(0, 0);
            this.txtChat.Multiline = true;
            this.txtChat.Name = "txtChat";
            this.txtChat.ReadOnly = true;
            this.txtChat.Size = new System.Drawing.Size(177, 351);
            this.txtChat.TabIndex = 0;
            // 
            // btnMsgSend
            // 
            this.btnMsgSend.Location = new System.Drawing.Point(4, 78);
            this.btnMsgSend.Name = "btnMsgSend";
            this.btnMsgSend.Size = new System.Drawing.Size(170, 23);
            this.btnMsgSend.TabIndex = 1;
            this.btnMsgSend.Text = "Send";
            this.btnMsgSend.UseVisualStyleBackColor = true;
            this.btnMsgSend.Click += new System.EventHandler(this.btnMsgSend_Click);
            // 
            // txtMsgSend
            // 
            this.txtMsgSend.Location = new System.Drawing.Point(4, 4);
            this.txtMsgSend.Multiline = true;
            this.txtMsgSend.Name = "txtMsgSend";
            this.txtMsgSend.Size = new System.Drawing.Size(170, 70);
            this.txtMsgSend.TabIndex = 0;
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 460);
            this.Controls.Add(this.spltGame_Msg);
            this.Name = "FormGame";
            this.Text = "Connect Four";
            this.Load += new System.EventHandler(this.FormGame_Load);
            this.spltGame_Msg.Panel1.ResumeLayout(false);
            this.spltGame_Msg.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltGame_Msg)).EndInit();
            this.spltGame_Msg.ResumeLayout(false);
            this.spltGame_Buttons.Panel1.ResumeLayout(false);
            this.spltGame_Buttons.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltGame_Buttons)).EndInit();
            this.spltGame_Buttons.ResumeLayout(false);
            this.spltChat_MsgSend.Panel1.ResumeLayout(false);
            this.spltChat_MsgSend.Panel1.PerformLayout();
            this.spltChat_MsgSend.Panel2.ResumeLayout(false);
            this.spltChat_MsgSend.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltChat_MsgSend)).EndInit();
            this.spltChat_MsgSend.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spltGame_Msg;
        private System.Windows.Forms.SplitContainer spltChat_MsgSend;
        private System.Windows.Forms.TextBox txtChat;
        private System.Windows.Forms.TextBox txtMsgSend;
        private System.Windows.Forms.Button btnMsgSend;
        private System.Windows.Forms.SplitContainer spltGame_Buttons;
        private System.Windows.Forms.Label lbl60;
        private System.Windows.Forms.Label lbl61;
        private System.Windows.Forms.Label lbl62;
        private System.Windows.Forms.Label lbl63;
        private System.Windows.Forms.Label lbl50;
        private System.Windows.Forms.Label lbl64;
        private System.Windows.Forms.Label lbl51;
        private System.Windows.Forms.Label lbl65;
        private System.Windows.Forms.Label lbl52;
        private System.Windows.Forms.Label lbl53;
        private System.Windows.Forms.Label lbl40;
        private System.Windows.Forms.Label lbl54;
        private System.Windows.Forms.Label lbl41;
        private System.Windows.Forms.Label lbl55;
        private System.Windows.Forms.Label lbl42;
        private System.Windows.Forms.Label lbl43;
        private System.Windows.Forms.Label lbl30;
        private System.Windows.Forms.Label lbl44;
        private System.Windows.Forms.Label lbl31;
        private System.Windows.Forms.Label lbl45;
        private System.Windows.Forms.Label lbl32;
        private System.Windows.Forms.Label lbl33;
        private System.Windows.Forms.Label lbl20;
        private System.Windows.Forms.Label lbl34;
        private System.Windows.Forms.Label lbl21;
        private System.Windows.Forms.Label lbl35;
        private System.Windows.Forms.Label lbl22;
        private System.Windows.Forms.Label lbl23;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl24;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl00;
        private System.Windows.Forms.Label lbl25;
        private System.Windows.Forms.Label lbl01;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl02;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl03;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Label lbl04;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl05;
        private System.Windows.Forms.Button btnCol7;
        private System.Windows.Forms.Button btnCol6;
        private System.Windows.Forms.Button btnCol5;
        private System.Windows.Forms.Button btnCol4;
        private System.Windows.Forms.Button btnCol3;
        private System.Windows.Forms.Button btnCol2;
        private System.Windows.Forms.Button btnCol1;
    }
}