﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace quatroEmLinha
{
    public partial class FormConnect : Form
    {
        public FormConnect Self { get; set; }
        public FormGame    Game { get; set; }

        public FormConnect()
        {
            this.Self = this;
            InitializeComponent();
        }

        public FormConnect(bool IsAlsoHost, int port) : this()
        {
            //Pre formatted constructor
            txtIP.Text = $"127.0.0.1";
            txtPort.Text = port.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            // If everything checks out, fire up the Game form
            if (!VerifyForm())
            {
                return;
            }
            
            IPAddress IP = IPAddress.Parse(txtIP.Text);
            int Port = int.Parse(txtPort.Text);

            Game = new FormGame(IP, Port, txtNick.Text);
            Game.ShowDialog();

            // After game form is done, close the player socket and the form
            Game.PlayerMe.ConnectionData.Socket.Close();

            this.Close();
        }

        /// <summary>
        /// Checks out if all fields are valid
        /// </summary>
        /// <returns>
        /// True if it they're valid
        /// False if not valid
        /// </returns>
        private bool VerifyForm()
        {
            IPAddress insertedIP;
            int port;

            if (!(IPAddress.TryParse(txtIP.Text, out insertedIP)))
            {
                return false;
            }
            if (!int.TryParse(txtPort.Text, out port) || port <= 1024 || port >= 65535)
            {
                return false;
            }
            if (string.IsNullOrEmpty(txtNick.Text))
            {
                return false;
            }
            return true;
        }
    }
}
