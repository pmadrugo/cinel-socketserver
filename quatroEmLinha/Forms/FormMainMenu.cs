﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace quatroEmLinha
{
    public partial class FormMainMenu : Form
    {
        public FormMainMenu()
        {
            InitializeComponent();
        }

        private void hostAGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Host a game Button
            FormServer formServer = new FormServer();

            formServer.ShowDialog();
        }

        private void connectToAGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Join a game button
            FormConnect formConnect = new FormConnect();

            formConnect.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // About button
            FormAbout frmAbout = new FormAbout();

            frmAbout.ShowDialog();
        }
    }
}
