﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using quatroEmLinha.Core;
using System.Net.Sockets;
using System.Threading;

namespace quatroEmLinha
{
    public partial class FormGame : Form
    {
        public Game             Board       { get; set; }
        public List<Client>     Players     { get; set; }
        public Client           PlayerMe    { get; set; }
        public ManualResetEvent IsConnected { get; set; }

        public FormGame(IPAddress IP, int Port, string NickName)
        {
            InitializeComponent();
            
            this.Board = new Game();
            this.Players = new List<Client>();
            this.PlayerMe = new Client();
            this.PlayerMe.ConnectionData.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.IsConnected = new ManualResetEvent(false);
            
            EstabilishConnection(IP, Port);
            IsConnected.WaitOne();

            this.Players.Add(PlayerMe);
            SendData("getID");
            SendData($"nick|{NickName}");
        }

        /// <summary>
        /// Estabilishes connection based on an indicated IP and port, attempts 5 times in 5 seconds, if it fails, closes down the form.
        /// </summary>
        /// <param name="ip">IP</param>
        /// <param name="port">Port</param>
        private void EstabilishConnection (IPAddress ip, int port)
        {
            int attempts = 0;
            while (attempts <= 5 || !PlayerMe.ConnectionData.Socket.Connected)
            {
                try
                {
                    PlayerMe.ConnectionData.Socket.BeginConnect(ip, port, new AsyncCallback(OnConnect), PlayerMe);
                }
                catch (SocketException ex)
                {
                    txtChat.AppendText($"Connection attempts: {++attempts}");

                    Thread.Sleep(1000);
                }
                catch (ObjectDisposedException ex)
                {
                    txtChat.AppendText($"Connection attempts: {++attempts}");

                    Thread.Sleep(1000);
                }
            }
            if (attempts > 5)
            {
                this.Close();
            }
        }

        /// <summary>
        /// When the client finishes connecting, commence data receiving
        /// </summary>
        /// <param name="IAR"></param>
        private void OnConnect(IAsyncResult IAR)
        {
            try
            {
                Client client = (Client)IAR.AsyncState;

                client.ConnectionData.Socket.EndConnect(IAR);
                client.ConnectionData.Socket.BeginReceive(client.ConnectionData.Buffer, 0, client.ConnectionData.Buffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), client);
                IsConnected.Set();
            }
            catch (SocketException ex)
            {

            }
            catch (ObjectDisposedException ex)
            {

            }
        }

        /// <summary>
        /// When data is received, begin data processing and restart receiving more data
        /// </summary>
        /// <param name="IAR"></param>
        private void OnReceive(IAsyncResult IAR)
        {
            try
            {
                Client client = (Client) IAR.AsyncState;

                int bytesRead = client.ConnectionData.Socket.EndReceive(IAR);

                client.ConnectionData.ReceivedData = Encoding.ASCII.GetString(client.ConnectionData.Buffer, 0, bytesRead);

                ProcessData(client.ConnectionData.ReceivedData);

                client.ConnectionData.Socket.BeginReceive(client.ConnectionData.Buffer, 0, client.ConnectionData.Buffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), client);
            }
            catch (SocketException ex)
            {

            }
            catch (ObjectDisposedException ex)
            {

            }
            catch (OperationCanceledException ex)
            {

            }
        }

        /// <summary>
        /// Sends data to the server
        /// </summary>
        /// <param name="data">Parsed string of data to send to server</param>
        public void SendData(string data)
        {
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            PlayerMe.ConnectionData.Socket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnSend), PlayerMe);
        }

        /// <summary>
        /// When data is send, terminate this async callback "cycle".
        /// </summary>
        /// <param name="IAR"></param>
        private void OnSend(IAsyncResult IAR)
        {
            try
            {
                Client client = (Client)IAR.AsyncState;

                int bytesSent = client.ConnectionData.Socket.EndSend(IAR);
            }
            catch (SocketException ex)
            {
                
            }
            catch (ObjectDisposedException ex)
            {

            }
            catch (OperationCanceledException ex)
            {

            }
        }

        /// <summary>
        /// String deserialization/parsing
        /// </summary>
        /// <param name="receivedData"></param>
        private void ProcessData(string receivedData)
        {
            string[] receivedDataSplit = receivedData.Split('|');
            if (receivedDataSplit.Length <= 0)
            {
                return;
            }

            // Parse first command and then act accordingly
            switch (receivedDataSplit[0])
            {
                case "missing": { MessageBox.Show($"Não pode haver apenas um jogador!"); } break;
                case "nope": { UnlockControls(); } break;
                case "win":
                {
                    if (Convert.ToInt32(receivedDataSplit[1]) == PlayerMe.ID)
                    {
                        if (MessageBox.Show($"Ganhasteee!!! :)") == DialogResult.OK)
                        {
                            this.Invoke(
                                new Action(
                                    () =>
                                     {
                                         this.Close();
                                     }
                                )
                            );
                        }
                    }
                    else
                    {
                        if (MessageBox.Show($"Perdeste :(") == DialogResult.OK)
                        {
                            this.Invoke(
                                new Action(
                                    () =>
                                    {
                                        this.Close();
                                    }
                                )
                            );
                        }
                    }
                } break;
                case "msg": { txtChat.Invoke(new Action(() => { txtChat.AppendText($"{receivedData.Substring(4)}\n"); })); } break;
                case "setID": { PlayerMe.ID = uint.Parse(receivedDataSplit[1]); } break;
                case "list": { UpdatePlayerList(receivedData.Substring(5)); } break;
                case "nick": { PlayerMe.NickName = receivedData.Substring(5); } break;
                case "turn": {
                    Board.CurrentTurn = Convert.ToInt32(receivedData.Substring(5));
                    if (Board.CurrentTurn == PlayerMe.ID)
                    {
                        UnlockControls();
                    }
                } break;
                default: {
                    // If none was found, assume game play
                    Board.Deserialize(receivedData);
                    UpdateTable();

                } break;
            }
        }

        /// <summary>
        /// Unlocks all gameplay related controls
        /// </summary>
        private void UnlockControls()
        {
            foreach (Control control in spltGame_Buttons.Panel2.Controls)
            {
                control.Invoke(new Action(() =>
                {
                    control.Enabled = true;
                }));
            }
        }

        /// <summary>
        /// Locks all gameplay related controls
        /// </summary>
        private void LockControls()
        {
            foreach (Control control in spltGame_Buttons.Panel2.Controls)
            {
                control.Invoke(new Action(() =>
                {
                    control.Enabled = false;
                }));
            }
        }

        /// <summary>
        /// Updates the player list with data from the server
        /// </summary>
        /// <param name="receivedData"></param>
        private void UpdatePlayerList(string receivedData)
        {
            Players.Clear();
            Players.Add(PlayerMe);
            foreach (string data in receivedData.Split('$'))
            {
                string[] player = data.Split('|');
                if (int.Parse(player[0]) != PlayerMe.ID)
                {
                    Players.Add(new Client()
                    {
                        ID = uint.Parse(player[0]),
                        NickName = player[1]
                    });
                }
            }
        }

        /// <summary>
        /// Updates the gameplay view, filling the table/labels to match game data
        /// </summary>
        private void UpdateTable()
        {
            foreach (Label label in this.spltGame_Buttons.Panel1.Controls)
            {
                int aux = 0;
                if (label.Name.StartsWith("lbl") && int.TryParse(label.Name.Substring(3), out aux))
                {
                    int x, y;
                    x = int.Parse(label.Name.Substring(3,1));
                    y = int.Parse(label.Name.Substring(4,1));

                    switch (Board.Board[x,y])
                    {
                        case 0:
                        {
                            label.Invoke(
                                new Action(
                                    () => 
                                    {
                                        label.Text = $"";
                                        label.ForeColor = Color.Black;
                                    } 
                                )
                            );
                            
                        } break;
                        case 1:
                        {
                            label.Invoke(
                                new Action(
                                    () =>
                                    {
                                        label.Text = $"O";
                                        label.ForeColor = Color.Red;
                                    }
                                )
                            );
                        } break;
                        case 2:
                        {
                            label.Invoke(
                                new Action(
                                    () =>
                                    {
                                        label.Text = $"O";
                                        label.ForeColor = Color.Yellow;
                                    }
                                )
                            );
                        } break;
                    }
                }
            }
        }

        private void btnMsgSend_Click(object sender, EventArgs e)
        {
            SendData($"msg|{txtMsgSend.Text}");
            txtMsgSend.Text = $"";
        }

        /// <summary>
        /// Execute gamemove by Player ID at Col.
        /// </summary>
        /// <param name="playerID">Who is playing</param>
        /// <param name="col">Column where to play</param>
        private void Play(int playerID, int col)
        {
            SendData($"{playerID}|{col}");
            new Thread(
                new ThreadStart(() =>
                {
                    LockControls();
                    Thread.Sleep(1000);
                    UnlockControls();
                })
            ).Start();
        }
        private void btnCol1_Click(object sender, EventArgs e)
        {
            Play((int)PlayerMe.ID, 0);
        }

        private void btnCol7_Click(object sender, EventArgs e)
        {
            Play((int)PlayerMe.ID, 6);
        }

        private void btnCol2_Click(object sender, EventArgs e)
        {
            Play((int)PlayerMe.ID, 1);
        }

        private void btnCol3_Click(object sender, EventArgs e)
        {
            Play((int)PlayerMe.ID, 2);
        }

        private void btnCol4_Click(object sender, EventArgs e)
        {
            Play((int)PlayerMe.ID, 3);
        }

        private void btnCol5_Click(object sender, EventArgs e)
        {
            Play((int)PlayerMe.ID, 4);
        }

        private void btnCol6_Click(object sender, EventArgs e)
        {
            Play((int)PlayerMe.ID, 5);
        }

        private void FormGame_Load(object sender, EventArgs e)
        {
            UpdateTable();
        }
    }
}
