﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using quatroEmLinha.Core;

namespace quatroEmLinha.Server
{
    public class Server
    {
        public Socket ServerSocket { get; private set; }    // Server socket
        public ServerOption Settings { get; set; }          // Object holding server settings

        public string           _VersionNumber      = $"0.7.20181204";
        
        #region Properties
        public List<Client> ClientsList { get; set; }       // Player list
        public Game         Game        { get; set; }       // Game object containing game mechanics

        #endregion

        public Server(uint Port)
        {
            Game = new Game();
            ClientsList = new List<Client>();
            Settings = new ServerOption();
            ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            Settings.Port = Port;
            Settings.MaxConnections = 2;
        }

        #region Error Handling

        /// <summary>
        /// Function to handle OnSocketExceptions
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="client"></param>
        public void OnSocketException(SocketException ex, Client client = null)
        {
            if (client != null)
            {
                RemoveConnection(client);
            }
        }

        /// <summary>
        /// Function to handle OnOperationCanceledExceptions
        /// </summary>
        /// <param name="ocEx"></param>
        /// <param name="client"></param>
        public void OnOperationCanceledException(OperationCanceledException ocEx, Client client = null)
        {
            if (client != null)
            {
                RemoveConnection(client);
            }
        }

        #endregion

        #region Connection Management

        /// <summary>
        /// Handles disconnections in a somewhat cleaner fashion
        /// </summary>
        /// <param name="client">Client to which the connection belongs</param>
        public void RemoveConnection(Client client)
        {
            new Thread(new ThreadStart(() =>
            {
                if (client.ConnectionData.Socket != null)
                {
                    try
                    {
                        client.ConnectionData.Socket.Shutdown(SocketShutdown.Both);

                        while (client.ConnectionData.Socket.Connected)
                        {
                            Thread.Sleep(100);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

                if (ClientsList.Contains(client))
                {
                    ClientsList.Remove(client);
                }
                
            })).Start();
        }

        #endregion

        #region Server Communication

        /// <summary>
        /// Commence server listening
        /// </summary>
        public void StartListen()
        {
            ServerSocket.Bind(new IPEndPoint(IPAddress.Any, (int)Settings.Port));
            ServerSocket.Listen(3);

            try
            {
                ServerSocket.BeginAccept(new AsyncCallback(OnAccept), null);
            }
            catch (SocketException ex)
            {
                OnSocketException(ex);
            }
            catch (OperationCanceledException ocEx)
            {
                OnOperationCanceledException(ocEx);
            }
        }

        /// <summary>
        /// On acquiring connection, proceed to accept the connection
        /// </summary>
        /// <param name="IAR"></param>
        private void OnAccept(IAsyncResult IAR)
        {
            Client client = new Client();

            try
            {
                Socket incomingSocket = ServerSocket.EndAccept(IAR);

                // If more than 2 players, cancel
                if (ClientsList.Count >= 2)
                {
                    ServerSocket.BeginAccept(new AsyncCallback(OnAccept), null);
                    return;
                }

                client.ConnectionData.Socket = incomingSocket;
                client.ConnectionData.RefreshRemoteEndPointConnection();

                SendToAll($"msg|A new player has joined." , client);

                ClientsList.Add(client);

                client.ConnectionData.Socket.BeginReceive(
                    client.ConnectionData.Buffer,
                    0,
                    client.ConnectionData.Buffer.Length,
                    SocketFlags.None,
                    new AsyncCallback(OnReceive),
                    client
                );

                ServerSocket.BeginAccept(new AsyncCallback(OnAccept), null);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Connection accepted, proceeding to receiving data
        /// </summary>
        /// <param name="IAR"></param>
        private void OnReceive(IAsyncResult IAR)
        {
            Client client = (Client)IAR.AsyncState;

            if (!client.ConnectionData.Socket.Connected)
            {
                RemoveConnection(client);
                return;
            }

            try
            {
                // Get data

                int     receivedDataSize = client.ConnectionData.Socket.EndReceive(IAR);
                byte[]  receivedData     = new byte[receivedDataSize];

                Array.Copy(client.ConnectionData.Buffer, receivedData, receivedDataSize);
                
                string request          = Encoding.ASCII.GetString(receivedData);
                
                // Process data
                string response         = ProcessGame(client, request);

                // Prepare response towards clients
                byte[]  sendData        = Encoding.ASCII.GetBytes(response);

                // Send data
                client.ConnectionData.Socket.BeginSend(
                    sendData,
                    0,
                    sendData.Length,
                    SocketFlags.None,
                    new AsyncCallback(OnSend),
                    client
                );

                // Receive data once more
                client.ConnectionData.Socket.BeginReceive(
                    client.ConnectionData.Buffer,
                    0,
                    client.ConnectionData.Buffer.Length,
                    SocketFlags.None,
                    new AsyncCallback(OnReceive),
                    client
                );
            }
            catch (SocketException ex)
            {
                OnSocketException(ex, client);
            }
            catch (OperationCanceledException ocEx)
            {
                OnOperationCanceledException(ocEx, client);
            }
        }
        
        /// <summary>
        /// Send to All function
        /// </summary>
        /// <param name="response"></param>
        /// <param name="sender"></param>
        private void SendToAll(string response, Client sender)
        {
            foreach (Client clientReceiver in ClientsList)
            {
                if (clientReceiver != sender)
                {
                    byte[] sendData = Encoding.ASCII.GetBytes(response);
                    clientReceiver.ConnectionData.Socket.BeginSend(sendData, 0, sendData.Length, SocketFlags.None, new AsyncCallback(OnSend), clientReceiver);
                }
            }
        }

        /// <summary>
        /// On sending data remotely
        /// </summary>
        /// <param name="IAR"></param>
        private void OnSend(IAsyncResult IAR)
        {
            Client client = (Client)IAR.AsyncState;

            try
            {
                client.ConnectionData.Socket.EndSend(IAR);
            }
            catch (SocketException ex)
            {
                OnSocketException(ex, client);
            }
            catch (OperationCanceledException ocEx)
            {
                OnOperationCanceledException(ocEx, client);
            }
        }
        #endregion

        /// <summary>
        /// Process the game for each client
        /// </summary>
        /// <param name="client">Client to which the connection belongs to</param>
        /// <param name="request">String containing the request done to the server</param>
        /// <returns>
        /// A formatted string to return to the client so it can be parsed and then reproduced locally
        /// </returns>
        private string ProcessGame(Client client, string request = null)
        {
            string[] requestSplit = request.Split('|');
            if (requestSplit[0] == "getID")
            {
                if (client.ID == 0)
                {
                    client.ID = ClientsList.Count != 0 ? ClientsList.Max((c) => c.ID) + 1 : 1;
                    return $"setID|{client.ID}";
                }
            }
            if (client.ID != 0)
            {
                
                if (requestSplit[0] == "msg")
                {
                    string msgReturn = $"msg|{client.NickName}: {request.Substring(4)}";

                    SendToAll(msgReturn, client);
                    return msgReturn;
                }
                if (requestSplit[0] == "nick")
                {
                    string nickName = request.Substring(5);

                    client.NickName = nickName;
                    return $"nick|{nickName}";
                }
                if (requestSplit[0] == "getPlayers")
                {
                    string listPlayers = $"list|";

                    foreach(Client Player in ClientsList)
                    {
                        listPlayers += $"{Player.ToString()}";
                    }

                    return listPlayers;
                }
                if (requestSplit[0] == "getTurn")
                {
                    return $"turn|{Game.CurrentTurn}";
                }
                if (!string.IsNullOrEmpty(requestSplit[0]) && ClientsList.Count < 2)
                {
                    return $"missing";
                }

                string result = Game.Deserialize(request);
                SendToAll(result, client);
                return result;
            }
            return $"nope";
        }
    }
}