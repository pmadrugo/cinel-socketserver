﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quatroEmLinha.Server
{
    public class ServerOption
    {
        private uint _Port;
        private uint _MaxConnections;

        /// <summary>
        /// Control variable for toggling Debug Mode (CURRENTLY UNUSED)
        /// </summary>
        public bool DebugMode { get; private set; }

        /// <summary>
        /// Control variable for maximum connections (CURRENTLY UNUSED)
        /// </summary>
        public uint MaxConnections
        {
            get { return _MaxConnections; }
            set
            {
                if (value > 1)
                {
                    _MaxConnections = value;
                }
            }
        }

        /// <summary>
        /// Control variable for server port
        /// </summary>
        public uint Port
        {
            get { return _Port; }
            set
            {
                if (value > 1024 || value < 65535)
                {
                    _Port = value;
                }
            }
        }

        /// <summary>
        /// Toggles DebugMode on (CURRENTLY UNUSED)
        /// </summary>
        public void DebugMode_Enable()
        {
            this.DebugMode = true;
        }

        /// <summary>
        /// Toggles DebugMode off (CURRENTLY UNUSED)
        /// </summary>
        public void DebugMode_Disable()
        {
            this.DebugMode = false;
        }
    }
}
