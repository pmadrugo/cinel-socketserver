﻿namespace quatroEmLinha.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Game
    {
        public int[,]   Board       { get; set; } // Array corresponding to the game's board
        public int      CurrentTurn { get; set; } // Current player turn

        public Game(int x, int y)
        {
            this.Board = new int[x, y];
            this.CurrentTurn = 1;
        }
        public Game() : this(7, 6) { }

        #region Win Conditions
        /// <summary>
        /// Checks what player wins
        /// </summary>
        /// <returns></returns>
        public int CheckWinner()
        {
            int winner = 0;

            for (int x = 0; x < Board.GetLength(0); x++)
            {
                for (int y = 0; y < Board.GetLength(1); y++)
                {
                    if (x < Board.GetLength(0) - 3)
                    {
                        winner = Verify_X(x, y);
                        if (winner != 0)
                        {
                            return winner;
                        }
                        if (y < Board.GetLength(1) - 3)
                        {
                            winner = Verify_UpRightCheck(x, y);
                        }
                        if (winner != 0)
                        {
                            return winner;
                        }
                        if (y > 2)
                        {
                            winner = Verify_DownRightCheck(x, y);
                        }
                        if (winner != 0)
                        {
                            return winner;
                        }
                    }
                    if (y < Board.GetLength(1) - 3)
                    {
                        winner = Verify_Y(x, y);
                    }
                    if (winner != 0)
                    {
                        return winner;
                    }
                }
            }

            return winner;
        }

        /// <summary>
        /// Check vertically
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private int Verify_Y(int x, int y)
        {
            int orig = Board[x,y];

            for (int i = y; i < y + 4; i++)
            {
                if (Board[x, y] != Board[x, i])
                {
                    return 0;
                }
            }
            return Board[x, y];
        }

        /// <summary>
        /// Check down right diagonally
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private int Verify_DownRightCheck(int x, int y)
        {
            int orig = Board[x,y];

            for (int i = 0; i < 4; i++)
            {
                if (Board[x,y] != Board[x+i,y-i])
                {
                    return 0;
                }
            }

            return Board[x, y];
        }

        /// <summary>
        /// Check up and right diagonally
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private int Verify_UpRightCheck(int x, int y)
        {
            int orig = Board[x,y];

            for (int i = 0; i < 4; i++)
            {
                if (Board[x, y] != Board[x + i, y + i])
                {
                    return 0;
                }
            }

            return Board[x, y];
        }

        /// <summary>
        /// Check Horizontal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private int Verify_X(int x, int y)
        {
            int orig = Board[x,y];

            for (int i = x; i < x + 4; i++)
            {
                if (Board[x, y] != Board[i, y])
                {
                    return 0;
                }
            }
            return Board[x, y];
        }
        #endregion

        #region Placement Instructions
        /// <summary>
        /// Checks if it is possible to place a piece at X column
        /// </summary>
        /// <param name="Pos">Column</param>
        /// <returns>
        /// Position if success
        /// -1 if can't
        /// </returns>
        private int CanPlace(int Pos)
        {
            if (!(Pos >= 0 && Pos < Board.GetLength(0)))
            {
                return -1;
            }

            for (int y = 0; y < Board.GetLength(1); y++)
            {
                if (Board[Pos, y] == 0)
                {
                    return y;
                }
            }
            return -1;
        }

        /// <summary>
        /// Places a piece at X position by the indicated Player
        /// </summary>
        /// <param name="Player">Player ID</param>
        /// <param name="Pos">Column</param>
        /// <returns></returns>
        private bool PlaceAt(int Player, int Pos)
        {
            int y = CanPlace(Pos);
            if (y > -1)
            {
                Board[Pos,y] = Player;
                return true;
            }
            return false;
        }
        #endregion

        #region String Parsing
        /// <summary>
        /// Returns a string of the action performed
        /// </summary>
        /// <param name="Player">Player ID</param>
        /// <param name="Pos">Column</param>
        /// <returns>
        /// String of the action performed
        /// </returns>
        public string   Serialize(int Player, string Pos)
        {
            return $"{Player}|{Pos}";
        }

        /// <summary>
        /// Performs actions ingame according to a preformatted string (Parser)
        /// </summary>
        /// <param name="text">String to parse</param>
        /// <returns>
        /// String of the action performed
        /// "nope" if no action can be performed
        /// </returns>
        public string   Deserialize(string text)
        {
            string[] textSplit = text.Split('|');

            int Player  = Convert.ToInt32(textSplit[0]);
            int Pos     = Convert.ToInt32(textSplit[1]);
            
            if (Player != CurrentTurn)
            {
                return $"nope";
            }

            if(PlaceAt(Convert.ToInt32(textSplit[0]), Convert.ToInt32(textSplit[1])))
            {
                if (Player == 1)
                {
                    CurrentTurn = 2;
                }
                else
                {
                    CurrentTurn = 1;
                }
                if (CheckWinner() != 0)
                {
                    return $"win|{CheckWinner()}";
                }
                return Serialize(Player, Pos.ToString());
            }
            else
            {
                return $"nope";
            }
        }
        #endregion
    }
}
