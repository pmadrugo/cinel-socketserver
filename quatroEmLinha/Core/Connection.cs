﻿namespace quatroEmLinha.Core
{
    using System.Net;
    using System.Net.Sockets;

    public class Connection
    {
        public IPAddress    IP              { get; set;             }
        public Socket       Socket          { get; set;             }
        public byte[]       Buffer          { get; set;             }
        public string       ReceivedData    { get; set;             }

        public static int   BufferSize      { get { return 512; }   }

        public Connection()
        {
            this.Buffer = new byte[BufferSize];
        }

        /// <summary>
        /// Gets IP from the socket and saves it in the object for further referencing even if the socket gets disconnected
        /// </summary>
        public void RefreshRemoteEndPointConnection()
        {
            IP = ((IPEndPoint)Socket.RemoteEndPoint).Address;
        }
    }
}
