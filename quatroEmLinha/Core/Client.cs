﻿namespace quatroEmLinha.Core
{
    public class Client
    {
        public uint ID { get; set; }
        public string NickName { get; set; }
        public Connection ConnectionData { get; set; }

        public Client()
        {
            this.ConnectionData = new Connection();
        }

        /// <summary>
        /// Override the string to return all data from the Client (except its connection related data)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{ID}|{NickName}$";
        }
    }
}
